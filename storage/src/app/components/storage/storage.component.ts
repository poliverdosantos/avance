import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.css']
})
export class StorageComponent implements OnInit {


  nombre!: string;
  nombre2!:string;

  constructor() { }

ngOnInit(): void {
    this.nombre = 'Mercedes Sosa';
    this.nombre2 = 'Pedro Mendez';
    //Guardar datos
    //setItem(clave, valor)
    // sessionStorage.setItem('nombre', this.nombre);

    //Recuperar datos
    //getItem(clave, valor)
    // let resultado = sessionStorage.getItem('nombre');
    // console.log(resultado);


    //remover un item del storage 

    //limpiar todo el storage
    // sessionStorage.clear()
    // sessionStorage.removeItem('nombre');
    // resultado = sessionStorage.getItem('nombre')
    // console.log(resultado);
    
    //guardar datos
    localStorage.setItem("this.nombre2",this.nombre2);

   //recuperar datos
    // let resultado = localStorage.getItem('nombre2');
    // console.log(resultado);
    
    //remover un item del local storage
      // removeItem(clave)
      // localStorage.removeItem('nombre2');
      // resultado = localStorage.getItem('nombre2');
      // console.log(resultado);

    //limpiar todo el storage
    // localStorage.clear();
    // resultado=localStorage.getItem('nombre2');
    // console.log(resultado);

  
  }
}
