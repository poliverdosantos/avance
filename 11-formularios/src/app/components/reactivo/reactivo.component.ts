import { Component, OnInit } from '@angular/core';
import { AbstractControlOptions, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidadoresService } from 'src/app/servicios/validadores.service';
// import { AbstractControl,FormControl, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-reactivo',
  templateUrl: './reactivo.component.html',
  styleUrls: ['./reactivo.component.css']
})


export class ReactivoComponent implements OnInit {
  get nombreNoValido(){
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched;
  }
  get apellidoNoValido(){
    return (this.forma.get('apellido')?.invalid && !this.forma.get('apellido')?.errors?.['noZaralai']) && this.forma.get('apellido')?.touched;
  }


  get correoNoValido(){
    return this.forma.get('correo')?.invalid && this.forma.get('correo')?.touched;
  }
  get distritoNoValido(){
    return this.forma.get('direccion.distrito')?.invalid && this.forma.get('direccion.distrito')?.touched;
  }
  get ciudadNoValido(){
    return this.forma.controls['direccion'].get('ciudad')?.invalid && this.forma.controls['direccion'].get('ciudad')?.touched;
  }

  get pasatiempos2() {
    return this.forma.get('pasatiempos') as FormArray;
  }

  get apellidoNoZaralai(){
    return this.forma.get('apellido')?.errors?.['noZaralai'];
  }

  get pass1NoValido(){
    return this.forma.get('pass1')?.invalid && this.forma.get('pass1')?.touched;
  }

  get pass2NoValido(){
    return this.forma.get('pass2')?.hasError('noEsIgual');
    // const pass1 = this.forma.get('pass1')?.value;
    // const pass2 = this.forma.get('pass2')?.value;
    //return (pass1 === pass2) ? false : true;
  }

  get usuarioNoValido(){
    return (this.forma.get('usuario')?.invalid && !this.forma.get('usuario')?.errors?.['existe']) && this.forma.get('usuario')?.touched;
  }

  get usuarioIgualJerjes(){
    return this.forma.get('usuario')?.errors?.['existe'];
  }





  forma!: FormGroup;
  constructor( private fb: FormBuilder,
              private validadores:ValidadoresService) {
    this.crearFormulario();
    //this.cargarDataAlFomulario();
    this.cargarDataAlFomulario2();
   }

  ngOnInit(): void {
  }


  crearFormulario(): void{
    this.forma =this.fb.group({
    //Valores del array:
      //1er valor: El valor por defecto que tendra
      //2do valor: Son los validadores sincronos
      //3er valor: Son los validadores asincronos
      nombre: ['',[Validators.required,Validators.minLength(4)]],
      apellido: ['', [Validators.required, Validators.minLength(4), Validators.pattern(/^[a-zA-zñÑ\s]+$/), this.validadores.noZaralai]],
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      usuario: ['', Validators.required, this.validadores.existeUsuario],
      pass1: ['', Validators.required],
      pass2: ['', Validators.required],
      direccion: this.fb.group({
      distrito: ['', Validators.required],
      ciudad: ['', Validators.required]
      }),
      pasatiempos: this.fb.array([
        [],[]
      ])
    },
     {
      validators: this.validadores.passwordsIguales('pass1','pass2')
    } as AbstractControlOptions
);
}

  cargarDataAlFomulario(): void{
    this.forma.setValue({
      nombre: 'Juan',
      apellido: 'Perez',
      correo: 'juan@gmail.com',
      direccion: {
        distrito: 'Central',
        ciudad: 'Oruro'
      }
    });
  }

  //Modificar formulario
  cargarDataAlFomulario2(): void{
    this.forma.patchValue({
      apellido: 'Perez'
    });
  }






    guardar(): void{
      console.log(this.forma.value);
      //Reset del formulario
      this.LimpiarFomulario();
    }

    LimpiarFomulario(){
    //this.forma.reset();
    this.forma.reset({
      nombre: 'Pedro'
    });

  }

    /*agregar pasatiempo */
    agregarpasatiempo(): void{
      this.pasatiempos2.push(this.fb.control(''));
    }

    /*norrar pasatiempo */
    borrarPasatiempo(i: number): void {
      this.pasatiempos2.removeAt(i);
    }
  
}
